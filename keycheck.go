package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/spf13/viper"
)

func test(kf string, password string) error {
	fmt.Println("testing ", password)
	file, err := os.Open(kf)

	if err != nil {
		fmt.Println("error opening keystore ", kf)
		os.Exit(1)
	}

	defer file.Close()
	_, err = bind.NewTransactor(bufio.NewReader(file), password)
	if err != nil {

		return err
	}
	fmt.Println(password, "could be it!")
	return nil
}

type password struct {
	Password string
}

func main() {
	var pws []password
	viper.SetConfigName("config") // name of config file (without extension)
	viper.AddConfigPath(".")      // optionally look for config in the working directory
	err := viper.ReadInConfig()   // Find and read the config file
	if err != nil {               // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	keyfile := viper.GetString("keyfile")
	pwfile := viper.GetString("passwords")
	file, err := os.Open(pwfile)
	if err != nil {
		log.Fatal("opening pw file ", err)
	}
	err = json.NewDecoder(bufio.NewReader(file)).Decode(&pws)
	if err != nil {
		log.Fatal("decoding pw file ", err)
	}
	for _, pw := range pws {
		if test(keyfile, pw.Password) == nil {
			break
		}
	}

}
